# Tutorial GIT #

## Setear credenciales para poder comitear en el repositorio

`git config --global user.name "tu_nombre"`

`git config --global user.email "mail_que_usas_en_bitbucket@example.com"`

## ir al test_repo de su home
`cd  ~/test_repo`

## crear rama `feature/tu_nombre`
ejecutar `git checkout -b feature/tu_nombre`

## modificar main y notebook
1. Agrega un metodo con el nombre de tu rama a `main.py`
2. Agrega un print con el nombre de tu rama en el método `main()` a main.py
3. Agrega el nombre de tu rama a la variable `branches list`

## Agrega los archivos que modificaste
1.  `git add main.py Test_Notebook.ipynb`
2.  `git commit -m "Modified main.py and Test_Notebook.ipynb`

## Ahora asegurate que tu rama este al día con develop
`git pull origin develop (o nbmerge)`

## Crea un pull request desde la página de bitbucket y agrega a alguien como reviewer
Si tu rama no tiene conflictos, el reviewer deberá aprobarla. En caso contrario será rechazada y debes hacer los cambios que te indiquen para luego crear otro pull request.
